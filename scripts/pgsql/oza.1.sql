------------------------------------------------------------------------------
-- Tablas para almacenamento de archivos OZA de MMS
------------------------------------------------------------------------------

INSERT INTO mms_d_module
VALUES ('OZA', 1); 

-- DROP TABLE mms_d_object_oza; 
------------------------------------------------------------------------------
CREATE TABLE mms_d_object_oza (
------------------------------------------------------------------------------
  id_object  INTEGER  NOT NULL,
  ob_oza     VARCHAR  NOT NULL,
  CONSTRAINT pk1_mms_d_object_oza
    PRIMARY KEY (id_object),
  CONSTRAINT fk1_mms_d_object_oza
    FOREIGN KEY (id_object) REFERENCES mms_d_object (id_object)
);
