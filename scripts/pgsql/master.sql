------------------------------------------------------------------------------
-- Tablas para almacenamento de los m�dulos de la BBDD de MMS
------------------------------------------------------------------------------

-- DROP TABLE mms_d_module;
------------------------------------------------------------------------------
CREATE TABLE mms_d_module (
------------------------------------------------------------------------------
  co_module   VARCHAR  NOT NULL,  
  nu_version  INTEGER  NOT NULL,
  CONSTRAINT pk1_mms_d_module
    PRIMARY KEY (co_module)
);
