------------------------------------------------------------------------------
-- Tablas b�sicas para el almacenamiento de objetos de MMS
------------------------------------------------------------------------------

-- INSERT INTO mms_d_module
-- VALUES ('BASE', 1); 

-- DROP TABLE mms_d_object; 
------------------------------------------------------------------------------
CREATE TABLE mms_d_object (
------------------------------------------------------------------------------
  id_object        INTEGER IDENTITY(1,1)  NOT NULL,
  co_subclass      VARCHAR(1024)    NOT NULL,
  co_name          VARCHAR(1024)    NOT NULL,
  co_version       VARCHAR(1024)    NOT NULL,
  ds_object        VARCHAR(1024),
  ds_tags          VARCHAR(1024), 
  dt_creation      DATETIME  NOT NULL,
  dt_modification  DATETIME  NOT NULL,
  CONSTRAINT pk1_mms_d_object
    PRIMARY KEY (id_object),
  CONSTRAINT pk2_mms_d_object
    UNIQUE (co_subclass, co_name, co_version)
); 	
